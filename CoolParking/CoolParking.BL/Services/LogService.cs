﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }
        public string LogPath { get; private set; }

        public string Read()
        {
            try
            {   // Open the text file using a stream reader.
                using (StreamReader sr = new(LogPath))
                {
                    // Read the stream to a string, and write the string to the console.
                    string log = sr.ReadToEnd();
                    sr.Close();
                    return log;
                }
            }
            catch (IOException e)
            {
                throw new System.InvalidOperationException("The log file does not exist!", e);
            }
        }

        public void Write(string logInfo)
        {
            using StreamWriter outputFile = new(LogPath, true);
            outputFile.WriteLine(logInfo);
            outputFile.Close();
        }
    }
}