﻿using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public class ToUpVehicleBalance
    {
        public ToUpVehicleBalance()
        {

        }
        [JsonPropertyName("id")]
        public string id { get; set; }
        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
