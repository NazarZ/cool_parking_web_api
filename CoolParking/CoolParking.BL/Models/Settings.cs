﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const int ParkingCapacity = 10;
        public const int StartParkingCapacity = 0;
        public const int PaymentPeriod = 5;
        public const int LoggingPeriod = 60;
        public const decimal FineRatio = (decimal)2.5;

        public static Dictionary<VehicleType, decimal> Tarrifs = new()
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, (decimal)3.5 },
            { VehicleType.Motorcycle, 1 }
        };
    }
}
