﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [Required]
        [JsonPropertyName("id")]
        public string Id { get; }

        [Required]
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; }

        [Required]
        [JsonPropertyName("balance")]
        public decimal Balance { get; internal set; }

        private static Random random = new Random();
        private static Parking parking = Parking.GetParking();

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = IsCorrectId(id) ? id : throw new ArgumentException($"Vehicle number {id} is not valid", "400");

            VehicleType = Enum.IsDefined(typeof(VehicleType), vehicleType) ? vehicleType : throw new ArgumentException("Vehicle Type is invalid", "400"); ;

            Balance = balance > 0 ? balance : throw new ArgumentException("Balance must be above a zero");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            while (true)
            {
                var newId = RandomCh() + RandomDigit() + RandomCh();
                if (!parking.Vehicles.Where(x => x.Id == newId).Any())
                {
                    return newId;
                }
            }
        }

        private static string RandomDigit()
        {
            return "-" + new string(Enumerable.Repeat("0123456789", 4)
             .Select(s => s[random.Next(s.Length)]).ToArray()) + "-";
        }

        private static string RandomCh()
        {
            return new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 2)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static bool IsCorrectId(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var buf = id.Split('-');
                if (buf.Length == 3)
                {
                    return Regex.IsMatch(buf[0], "^[A-Z]{2}$") && Regex.IsMatch(buf[2], "^[A-Z]{2}$") && Regex.IsMatch(buf[1], "^[0-9]{4}$");
                }
                else
                {
                    return false;
                }
            }
            return false;
           
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Vehicle otherVehicle = obj as Vehicle;
            if (otherVehicle != null)
            {
                return this.Id.Equals(otherVehicle.Id);
            }
            else
            {
                throw new ArgumentException("Type of object isn't valid");
            }
        }
    }
}