﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Text.Json.Serialization;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        [JsonPropertyName("transactionDate")]
        public DateTime TransactionTime { get; private set; }
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal VehicleBalance { get; set; }


        public TransactionInfo()
        {
            TransactionTime = DateTime.Now;
        }
    }
}