﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Uniques<T> : HashSet<T>
    {
        public Uniques()
        {
        }

        public Uniques(IEnumerable<T> collection)
        {
            AddRange(collection);
        }

        public new bool Add(T item)
        {
            if (!base.Add(item))
            {
                throw new ArgumentException("The vehicle already exists");
            }
            else
            {
                return true;
            }
        }

        public new int RemoveWhere(Predicate<T> predicate)
        {
            var i = base.RemoveWhere(predicate);
            if (i == 0)
            {
                throw new ArgumentException("The vehicle does not exist");
            }
            else
            {
                return i;
            }
        }

        public void AddRange(IEnumerable<T> collection)
        {
            foreach (T item in collection)
            {
                Add(item);
            }
        }
    }
}