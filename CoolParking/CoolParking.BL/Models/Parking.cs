﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    internal class Parking
    {
        private static Parking _instance;
        private static readonly object _syncRoot = new();

        public List<TransactionInfo> Transaction { get; set; }
        public decimal ParkingBalance { get; set; }
        internal decimal LastParkingBalance { get; set; }
        public Uniques<Vehicle> Vehicles { get; set; }

        private Parking()
        {
            Vehicles = new Uniques<Vehicle>();
            Transaction = new List<TransactionInfo>();
        }
        public static Parking GetParking()
        {
            if (_instance == null)
            {
                lock (_syncRoot)
                {
                    if (_instance == null)
                        _instance = new Parking();
                }
            }
            return _instance;
        }

        public void RemoveVehicleById(string vehicleId)
        {
            var v = Vehicles.FirstOrDefault(x => x.Id == vehicleId);
            if (v != null)
            {
                if (v.Balance >= 0)
                {
                    Vehicles.Remove(v);
                }
                else
                {
                    throw new InvalidOperationException("The balance must be greater than or equal to 0");
                }
            }
            else
            {
                throw new ArgumentException("The vehicle does not exist");
            }
        }

        public void ToUpBalanceVehicle(string vehicleId, decimal sum)
        {
            var Vehicle = Vehicles.FirstOrDefault(x => x.Id == vehicleId);
            if (Vehicle != null)
            {
                Vehicle.Balance += sum;
            }
            else
            {
                throw new ArgumentException("The vehicle does not exist");
            }
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - Vehicles.Count;
        }
    }
}

