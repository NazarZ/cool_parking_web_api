﻿namespace CoolParking.UI.Console.WebAPI
{
    using CoolParking.BL.Models;
    using System;
    using System.Text;

    class Program
    {
        private static WebAPIParking parking;
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            parking = new WebAPIParking("https://localhost:44324/");
            MainMenu();
        }

        public static void MainMenu()
        {
            Console.WriteLine("WELCOME TO THE MAIN MENU");
            Console.WriteLine("\t1 - Вивести на екран поточний баланс Паркінгу");
            Console.WriteLine("\t2 - Вивести на екран суму зароблених коштів за поточний період (до запису у лог)");
            Console.WriteLine("\t3 - Вивести на екран кількість вільних місць на паркуванні");
            Console.WriteLine("\t4 - Вивести на екран усі Транзакції Паркінгу за поточний період");
            Console.WriteLine("\t5 - Вивести на екран історію Транзакцій");
            Console.WriteLine("\t6 - Вивести на екран список Тр. засобів , що знаходяться на Паркінгу");
            Console.WriteLine("\t7 - Поставити Транспортний засіб на Паркінг");
            Console.WriteLine("\t8 - Забрати Транспортний засіб з Паркінгу");
            Console.WriteLine("\t9 - Поповнити баланс конкретного Тр. засобу");
            Console.WriteLine("To quit from the program - enter 0\n");
            int userChoice = Convert.ToInt32(Console.ReadLine());
            switch (userChoice)
            {
                case 0:
                    Environment.Exit(0);
                    break;
                case 1:
                    GetParkingCurrentBalance();
                    break;
                case 2:
                    GetParkingBalanceBeforeLogs();
                    break;
                case 3:
                    GetFreePlaces();
                    break;
                case 4:
                    GetLastParkingTransactions();
                    break;
                case 5:
                    ShowTransationsLogFile();
                    break;
                case 6:
                    GetVehicles();
                    break;
                case 7:
                    AddVehicle();
                    break;
                case 8:
                    DeleteVehicle();
                    break;
                case 9:
                    TopUpVehicle();
                    break;
                default:
                    MainMenu();
                    break;
            }
        }

        // 1. Вивести на екран поточний баланс Паркінгу
        public static void GetParkingCurrentBalance()
        {
            try
            {
                Console.WriteLine($"\nParking Current Balance is {parking.GetBalanceAsync().Result}\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 2. Вивести на екран суму зароблених коштів за поточний період (до запису у лог)
        public static void GetParkingBalanceBeforeLogs()
        {
            try
            {
                Console.WriteLine($"\nParking balance before logs: {parking.GetBalanceBeforeLogAsync().Result}\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 3. Вивести на екран кількість вільних місць на паркуванні (вільно X з Y) 
        public static void GetFreePlaces()
        {
            try
            {
                Console.WriteLine($"\nFree places: {parking.GetFreePlaces().Result}/{parking.GetCapacityAsync().Result}\n");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 4. Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог)
        public static void GetLastParkingTransactions()
        {
            Console.WriteLine($"\nLast Transactions before Logs:\n");
            try
            {
                foreach (var i in parking.GetLastTransactionsAsync().Result)
                {
                    Console.WriteLine($"{i.TransactionTime} {i.VehicleId} {i.Sum}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 5. Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log) 
        public static void ShowTransationsLogFile()
        {
            try
            {
                Console.Write(parking.GetAllTransactionsAsync().Result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 6. Вивести на екран список Тр.засобів , що знаходяться на Паркінгу
        public static void GetVehicles()
        {
            try
            {
                foreach (var i in parking.GetVehiclesAsync().Result)
                {
                    Console.WriteLine($"{i.Id} {i.VehicleType} {i.Balance}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        // 7. Поставити Транспортний засіб на Паркінг
        public static void AddVehicle()
        {
            Console.WriteLine("\nChoose a vehicle type:");
            Console.WriteLine("\t1 - Bus");
            Console.WriteLine("\t2 - Passenger Car");
            Console.WriteLine("\t3 - Truck");
            Console.WriteLine("\t4 - Motorcycle");
            int intVehicleType = Convert.ToInt32(Console.ReadLine());
            VehicleType _type = 0;
            switch (intVehicleType)
            {
                case 1:
                    Console.Write("Your vehicle is a Bus");
                    _type = VehicleType.Bus;
                    break;
                case 2:
                    Console.Write("Your vehicle is a Passenger Car");
                    _type = VehicleType.PassengerCar;
                    break;
                case 3:
                    Console.Write("Your vehicle is a Truck");
                    _type = VehicleType.Truck;
                    break;
                case 4:
                    Console.Write($"Your vehicle is a Motorcycle");
                    _type = VehicleType.Motorcycle;
                    break;
                default:
                    MainMenu();
                    break;
            }

            Console.WriteLine("\nEnter a vehicle number:");
            string vehicleNumber = Console.ReadLine();

            Console.WriteLine("Enter a balance:");
            string vehicleBalance = Console.ReadLine();

            try
            {
                var newVehicle = new Vehicle(vehicleNumber, _type, Convert.ToDecimal(vehicleBalance));
                var n = parking.AddVehiclesAsync(new Vehicle[] { newVehicle }).Result;
                Console.WriteLine("A vehicle has been added");
                foreach (var i in n)
                {
                    Console.WriteLine($"{i.Id} {i.VehicleType.ToString()} {i.Balance}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Do you want to add another vehicle? If yes - enter 'y', no - any other character.");
                if (Console.ReadLine() == "y")
                {
                    AddVehicle();
                }
            }
            finally
            {
                MainMenu();
            }
        }

        // 8. Забрати Транспортний засіб з Паркінгу
        public static void DeleteVehicle()
        {
            Console.WriteLine("Enter a vehicle number to delete");
            string vehicleNumber = Console.ReadLine();
            try
            {
                if (parking.DeleteVehicleAsync(vehicleNumber).Result)
                {
                    Console.WriteLine("A vehicle has been removed from parking");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Do you want to remove another vehicle? If yes - enter 'y', no - any other character.");
                if (Console.ReadLine() == "y")
                {
                    DeleteVehicle();
                }
            }
            finally
            {
                MainMenu();
            }
        }

        // 9. Поповнити баланс конкретного Тр. засобу
        public static void TopUpVehicle()
        {
            Console.WriteLine("Enter a vehicle number to top up");
            string vehicleNumber = Console.ReadLine();
            Console.WriteLine("Enter a sum");
            decimal sum = Convert.ToDecimal(Console.ReadLine());
            try
            {
                var i = parking.TopUpVehicleAsync(new ToUpVehicleBalance() { id = vehicleNumber, Sum = sum }).Result;
                Console.WriteLine($"A vehicle has been toped up {i.Id} {i.VehicleType.ToString()} {i.Balance}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Do you want to top up another vehicle? If yes - enter 'y', no - any other character.");
                if (Console.ReadLine() == "y")
                {
                    TopUpVehicle();
                }
            }
            finally
            {
                MainMenu();
            }
        }
    }
}
