﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking.UI.Console.WebAPI
{
    class WebAPIParking
    {
        private HttpClient client = new HttpClient();
        public WebAPIParking(string uri)
        {
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Vehicle>> GetVehiclesAsync()
        {
            List<Vehicle> vehicles = null;
            HttpResponseMessage response = await client.GetAsync("api/vehicles");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                vehicles = JsonSerializer.Deserialize<List<Vehicle>>(i);
            }
            return vehicles;
        }
        public async Task<Vehicle> GetVehicleByIdAsync(string id)
        {
            Vehicle vehicle = null;
            HttpResponseMessage response = await client.GetAsync("api/vehicles/" + id);
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                vehicle = JsonSerializer.Deserialize<Vehicle>(i);
            }
            return vehicle;
        }
        public async Task<List<Vehicle>> AddVehiclesAsync(Vehicle[] Invehicles)
        {
            List<Vehicle> vehicles = null;
            using (var content = new StringContent(JsonSerializer.Serialize(Invehicles), System.Text.Encoding.UTF8, "application/json"))
            {
                HttpResponseMessage response = await client.PostAsync("/api/vehicles", content);
                if (response.IsSuccessStatusCode)
                {
                    var i = await response.Content.ReadAsStringAsync();
                    vehicles = JsonSerializer.Deserialize<List<Vehicle>>(i);
                }
            }
            return vehicles;
        }

        public async Task<bool> DeleteVehicleAsync(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync("api/vehicles/" + id);
            return response.IsSuccessStatusCode;
        }

        public async Task<decimal> GetBalanceAsync()
        {
            decimal balance = -1;
            HttpResponseMessage response = await client.GetAsync("api/Parking/balance");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                balance = JsonSerializer.Deserialize<decimal>(i);
            }
            return balance;
        }
        public async Task<decimal> GetBalanceBeforeLogAsync()
        {
            decimal balance = -1;
            HttpResponseMessage response = await client.GetAsync("api/Parking/balanceBeforeLog");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                balance = JsonSerializer.Deserialize<decimal>(i);
            }
            return balance;
        }
        public async Task<int> GetCapacityAsync()
        {
            int Capacity = -1;
            HttpResponseMessage response = await client.GetAsync("api/Parking/capacity");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                Capacity = JsonSerializer.Deserialize<int>(i);
            }
            return Capacity;
        }
        public async Task<int> GetFreePlaces()
        {
            int free = -1;
            HttpResponseMessage response = await client.GetAsync("api/parking/freePlaces");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                free = JsonSerializer.Deserialize<int>(i);
            }
            return free;
        }
        public async Task<List<TransactionInfo>> GetLastTransactionsAsync()
        {
            List<TransactionInfo> transactions = null;
            HttpResponseMessage response = await client.GetAsync("api/transactions/last");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                transactions = JsonSerializer.Deserialize<List<TransactionInfo>>(i);
            }
            return transactions;
        }
        public async Task<string> GetAllTransactionsAsync()
        {
            string transactions = null;
            HttpResponseMessage response = await client.GetAsync("api/transactions/all");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                transactions = JsonSerializer.Deserialize<string>(i);
            }
            return transactions;
        }
        public async Task<Vehicle> TopUpVehicleAsync(ToUpVehicleBalance toUpVehicle)
        {
            Vehicle vehicle = null;
            using (var content = new StringContent(JsonSerializer.Serialize(toUpVehicle), System.Text.Encoding.UTF8, "application/json"))
            {
                HttpResponseMessage response = await client.PutAsync("api/transactions/topUpVehicle", content);
                if (response.IsSuccessStatusCode)
                {
                    var i = await response.Content.ReadAsStringAsync();
                    vehicle = JsonSerializer.Deserialize<Vehicle>(i);
                }
            }
            return vehicle;
        }
    }
}
