﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/Vehicles
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            Response.ContentType = "application/json";
            return Ok(_parkingService.GetVehicles());
        }

        // GET: api/Vehicle/id
        [HttpGet("{id}", Name = "Get")]
        public ActionResult Get(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                return Ok(_parkingService.GetVehicleById(id));
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }

        // POST: api/Vehicle
        [HttpPost]
        public ActionResult<Vehicle> Post([FromBody] Vehicle vehicle)
        {
            try
            {
                if (vehicle != null)
                {
                    _parkingService.AddVehicle(new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance));
                    Response.ContentType = "application/json";
                    return Created("", vehicle);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // DELETE: api/Vehicle/id
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                if (!Vehicle.IsCorrectId(id))
                {
                    throw new ArgumentException($"Vehicle number {id} is not valid", "400");
                }
                Response.ContentType = "application/json";
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
