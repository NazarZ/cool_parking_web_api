﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;
        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/Parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet("balanceBeforeLog")]
        public ActionResult<decimal> GetBalanceBeforLog()
        {
            return Ok(_parkingService.GetBalanceBeforeLogs());
        }

        // GET: api/Parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        // GET api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}
