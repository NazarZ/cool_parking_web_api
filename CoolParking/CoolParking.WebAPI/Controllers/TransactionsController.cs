﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;
        public TransactionsController(IParkingService parking)
        {
            _parkingService = parking;
        }

        // GET api/transactions/last
        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            Response.ContentType = "application/json";
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                Response.ContentType = "application/json";
                return Ok(_parkingService.ReadFromLog());
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        //PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> Put([FromBody] ToUpVehicleBalance toUp)
        {
            try
            {
                if (toUp != null)
                {
                    if (!Vehicle.IsCorrectId(toUp.id))
                    {
                        throw new ArgumentException($"Vehicle number {toUp.id} is not valid", "400");
                    }
                    _parkingService.TopUpVehicle(toUp.id, toUp.Sum);
                    Response.ContentType = "application/json";
                    return Ok(_parkingService.GetVehicleById(toUp.id));
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }
    }
}
